//---------------------------------------------------------------------------
#include <vcl.h>
#include <vfw.h>
#pragma hdrstop

#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <conio.h>
#include <time.h>
#include <string.h>
#include <math.h>


#include "ihm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//VARI�VEL GLOBAL
HANDLE hCOMM;
HWND w;
bool chave = false;
int col0,lin0,col1,lin1,chavemouseesquerda;

//CONFIGURA��O DA PORTA
char porta[100],configuracao[100];

//CONFIGURACAO DA PORTA DE COMUNICADO - CHAMAR NO FORMCREATE
void ConfigurarPorta(char *com,char *confg)
{
  hCOMM = CreateFile(com,GENERIC_READ | GENERIC_WRITE,0,0,OPEN_EXISTING,0,0);
  if (hCOMM==INVALID_HANDLE_VALUE)
  {
    ShowMessage("Erro - Porta de Comunica��o");
  }
  else
    {
      DCB dcbCOMM;
      dcbCOMM.DCBlength=sizeof(DCB);
      GetCommState(hCOMM,&dcbCOMM);
      BuildCommDCB(confg,&dcbCOMM);
      SetCommState(hCOMM,&dcbCOMM);
     }
}
//TRANSMITIR 1 CARACTER
void TransmitirCaracter(char c)
{
  TransmitCommChar(hCOMM,c);
}


//TRANSMITIR 1 STRING
void TransmitirString(char *c)
{
 for(long int i=0; i< strlen(c); i++)
   {
    if ( *(c+i)!=' ')
     TransmitCommChar(hCOMM,*(c+i));
    else
     TransmitCommChar(hCOMM,'\r');
    }
}


void Atraso(long int tt)
{
 struct  time t;
 float tempo1,tempo2=10000;

 //HORA ATUAL
 gettime(&t);
 tempo1=(t.ti_hour*3600.+t.ti_min*60.+t.ti_sec+t.ti_hund/100.)*1000.;
 while (tempo2-tempo1<tt)
   {
    gettime(&t);
    tempo2=(t.ti_hour*3600.+t.ti_min*60.+t.ti_sec+t.ti_hund/100.)*1000.;
    }
}


//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
Close();
}


void __fastcall TForm1::FormCreate(TObject *Sender)
{
  CloseHandle(hCOMM);
  //COMO CHAMAR A ROTINA
  char porta[1000];
  //DEFINIR PORTA
  strcpy(porta,"COM1");
  //CONFIGURAR PORTA
  strcpy(configuracao,"9600,N,8,1");
  //SETAR PORTA
  ConfigurarPorta(porta,configuracao);
   col0=0;
   lin0=0;
   col1=0;
   lin1=0;
   chavemouseesquerda = 0;
   ScrollBar1->Position = 139;
   ScrollBar2->Position = 138;
}
//-------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  char aux[1000];
  sprintf(aux,"%s\r",Edit1->Text);
  TransmitirString(aux);        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::CadastrarClick(TObject *Sender)
{
  char aux[1000];
  char aux2[1000];
  sprintf(aux, "A%d B%d C%d",ScrollBar1->Position,ScrollBar2->Position);
  ListBox1->Items->Add(aux);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ScrollBar2Change(TObject *Sender)
{
   char aux[1000];
   sprintf(aux, "A%d\r",ScrollBar2->Position);
   TransmitirString(aux);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ScrollBar1Change(TObject *Sender)
{
   char aux[1000];
   sprintf(aux, "B%d\r",ScrollBar1->Position);
   TransmitirString(aux);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
 char aux[1000];
 int deltacol,deltalin;

 //sprintf(aux,"%d  %d",X,Y);
 sprintf(aux,"O Pacificador 0.1b");
 Form1->Caption=aux;
 col0 = col1;
 lin0 = lin1;

 col1 = X;
 lin1 = Y;

 deltacol = col1 - col0;
 deltalin = lin1 - lin0;


 if(chavemouseesquerda == 1){
   
        if(deltalin > 0){
                ScrollBar2->Position=ScrollBar2->Position + 1;
        }else{
                ScrollBar2->Position=ScrollBar2->Position - 1;
        }
        if(deltacol > 0){
                ScrollBar1->Position=ScrollBar1->Position + 1;
        }else{
                ScrollBar1->Position=ScrollBar1->Position - 1;
     }
 }

}


//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
chavemouseesquerda = 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
 chavemouseesquerda = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::WebcamClick(TObject *Sender)
{
if(chave == false){
w = capCreateCaptureWindow("Meu Video", WS_CHILD | WS_VISIBLE,
1, 1, 320, 240, Camera->Handle, 0);

int iCapDrv;
for (iCapDrv=0; iCapDrv<10; iCapDrv++)
if (capDriverConnect(w, iCapDrv))
break;

if (iCapDrv >= 10)
throw Exception("Erro: N�o foram encontrados dispositivos.");
else chave = true;

// Aqui poder�amos chamar a fun��o a seguir
// para selecionar a resolu��o em tempo de execu��o
// capDlgVideoFormat(w);

capPreview(w, true);
capPreviewScale(w, true);
capPreviewRate(w, 1);
}else{
throw Exception("Erro: Webcam ja est� ligada!");
}

}
//---------------------------------------------------------------------------




void __fastcall TForm1::Image1MouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
 FormMouseMove(Sender,Shift,X,Y);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Image1MouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
 FormMouseUp(Sender, Button,Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Image1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
 FormMouseDown (Sender, Button,Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
 CadastrarClick(Sender);
 TransmitirString("C110\r");
 Atraso(500);
 TransmitirString("C90\r");
 printf("%i",ScrollBar1->Position);
 //printf("%i",ScrollBar2->Position);

}
//---------------------------------------------------------------------------



void __fastcall TForm1::CameraMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   FormMouseDown(Sender, Button,Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CameraMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
 FormMouseMove(Sender,Shift,X,Y);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CameraMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
 FormMouseUp(Sender, Button,Shift, X, Y);        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::GravarClick(TObject *Sender)
{
   FILE *arq;
   char aux[100000];

   sprintf(aux,"%s","pacificador.txt");
   arq=fopen(aux,"wt");
   for(long int i=0; i<ListBox1->Items->Count;i++)
     {
       fprintf(arq,"%s\n",ListBox1->Items->Strings[i]);
      }
   fclose(arq);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ClearClick(TObject *Sender)
{
 ListBox1->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LerClick(TObject *Sender)
{
   FILE *arq;
   char aux[100000];

   sprintf(aux,"%s","pacificador.txt");
   arq=fopen(aux,"rt");
   ListBox1->Clear();
   while(!feof(arq))
     {
       fgets(aux,10000,arq);
       aux[strlen(aux)-1]='\0';
       if (!feof(arq)) ListBox1->Items->Add(aux);
      }
   fclose(arq);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TudoClick(TObject *Sender)
{
   char aux[100000];
   long int atraso,j;
   long int rpt=1;
 for(j=0;j<rpt;j++)
  {
     atraso=1000;
     for(long int i=0; i<ListBox1->Items->Count;i++)
       {
         sprintf(aux,"%s\r",ListBox1->Items->Strings[i]);
         TransmitirString(aux);
         Atraso(atraso);
        }
  }
}
//---------------------------------------------------------------------------


